const {parking} = require('../models')
const { Op } = require('sequelize');
const moment = require('moment')

module.exports = {
    add_parking: async (req, res) => {
        if (req.body.waktu_masuk < req.body.waktu_keluar) {

            var biaya_parkir_perhari = 0
            var biaya_parkir_perjam = 0
            if (req.body.jenis_kendaraan == 'mobil') {
                biaya_parkir_perjam = 5000
                biaya_parkir_perhari = 80000
            }else{
                biaya_parkir_perjam = 2000
                biaya_parkir_perhari = 40000
            }

            diff = new Date(req.body.waktu_keluar) - new Date(req.body.waktu_masuk)
            diff_in_minute = diff/(60*1000)
            diff_in_hours = diff_in_minute/60

            var tagihan = 0
            var lama_parkir = 0

            console.log(diff);
            console.log(diff_in_minute);
            console.log(diff_in_hours);
            if (diff_in_minute >= (60*24)) {
                lama_parkir = ((diff_in_minute / 60) / 24).toFixed(0)
                tagihan = lama_parkir * biaya_parkir_perhari
            }
            if ((diff_in_hours % 24) > 0) {
                tagihan += (Math.floor(diff_in_hours%24) * biaya_parkir_perjam )
            }
            if ((diff_in_minute % 60) >= 1) {
                tagihan += biaya_parkir_perjam
            }
            console.log(tagihan);
            
            await parking.create({
                jenis_kendaraan:req.body.jenis_kendaraan,
                waktu_masuk:req.body.waktu_masuk,
                waktu_keluar:req.body.waktu_keluar,
                tagihan:tagihan
            })
            .then(resp => {
                res.status(201).redirect('/show_data');
            })
            .catch(err => {
                res.json({
                    status:403,
                    message:'failed insert data ',err
                })
            })
                
        } else{
            res.status(201).render('tambah_data', { isFalse : true } );
        }
    },

    show_data: async (req, res) => {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
       await parking.findAll({
            raw : true ,
            nest: true 
        })
        .then(resp => {
            res.status(201).render('data_parkir', { parking:resp, startDate:today, endDate:today } );

        })
        .catch(err => {
            res.status(500)
            res.json({
                message:err.message,
            })
        })
    },

    sort: async (req, res) => {
        waktu_masuk = new Date(req.params.waktu_masuk)
        waktu_keluar = new Date(req.params.waktu_keluar)
        if (req.params.jenis_kendaraan == "null" && req.params.harga_terendah == "null" && req.params.harga_teringgi == "null") {
            await parking.findAll({
                raw : true ,
                nest: true,
                where:{
                    waktu_masuk: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    
                    waktu_keluar: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                }
            }).then(resp => {
                console.log(resp);
                res.status(201).render('data_parkir', { parking : resp } );
    
            })
            .catch(err => {
                res.status(500)
                res.json({
                    message:err.message,
                })
            })
        }else if(req.params.jenis_kendaraan == "null" && req.params.harga_terendah != "null"){
            await parking.findAll({
                raw : true ,
                nest: true,
                where:{
                    waktu_masuk: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    
                    waktu_keluar: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                      tagihan: {
                        [Op.gte]: req.params.harga_terendah,
                      }
                }
            }).  then(resp => {
                console.log(resp);
                res.status(201).render('data_parkir', { parking : resp } );
    
            })
            .catch(err => {
                res.status(500)
                res.json({
                    message:err.message,
                })
            })
        } else if(req.params.jenis_kendaraan == 'null' && req.params.harga_teringgi != 'null'){
            await parking.findAll({
                raw : true ,
                nest: true,
                where:{
                    waktu_masuk: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    
                    waktu_keluar: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                      tagihan: {
                        [Op.lte]: req.params.harga_teringgi,
                      }
                }
            }).  then(resp => {
                console.log(resp);
                res.status(201).render('data_parkir', { parking : resp } );
    
            })
            .catch(err => {
                res.status(500)
                res.json({
                    message:err.message,
                })
            })
        } else if (req.params.jenis_kendaraan == 'null' && req.params.harga_teringgi != 'null' && req.params.harga_terendah != 'null'){
            await parking.findAll({
                raw : true ,
                nest: true,
                where:{
                    waktu_masuk: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    
                    waktu_keluar: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                      tagihan: {
                        [Op.between]: [harga_terendah ,req.params.harga_teringgi],
                      }
                }
            }).  then(resp => {
                console.log(resp);
                res.status(201).render('data_parkir', { parking : resp } );
    
            })
            .catch(err => {
                res.status(500)
                res.json({
                    message:err.message,
                })
            })
        } else if (req.params.jenis_kendaraan != 'null' ){
            await parking.findAll({
                raw : true ,
                nest: true,
                
                where:{
                    waktu_masuk: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    
                    waktu_keluar: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    jenis_kendaraan:req.params.jenis_kendaraan || null
                }
            }).then(resp => {
                console.log(resp);
                res.status(201).render('data_parkir', { parking : resp } );
    
            })
            .catch(err => {
                res.status(500)
                res.json({
                    message:err.message,
                })
            })
        }
        else{
            await parking.findAll({
                raw : true ,
                nest: true,
                
                where:{
                    waktu_masuk: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    
                    waktu_keluar: {
                        [Op.between]: [ moment(waktu_masuk.toISOString()), moment(waktu_keluar.toISOString()) ]
                      } || null,
                    jenis_kendaraan:req.params.jenis_kendaraan || null
                }
            }).then(resp => {
                console.log(resp);
                res.status(201).render('data_parkir', { parking : resp } );
    
            })
            .catch(err => {
                res.status(500)
                res.json({
                    message:err.message,
                })
            })
        }
      
    }

}