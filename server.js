const parking = require('./controllers/parking')
const express = require("express");

const app = express()
app.use(express.json())

app.set('view engine', 'ejs')
app.use(express.urlencoded({extended:false}))

app.listen(3300, ()=>{
    console.log("Sever is now listening at port 3300");
})

app.get('/', async (req, res) => {
    res.render('home')
})

app.get('/tambah_data', async (req, res) => {
    res.render('tambah_data',{isFalse: false})
})

app.post("/add_parking", parking.add_parking)
app.get("/show_data", parking.show_data)

app.get("/sort/:jenis_kendaraan/:waktu_masuk/:waktu_keluar/:harga_terendah/:harga_tertinggi", parking.sort)
